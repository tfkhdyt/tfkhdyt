# [![tfkhdyt's header](https://i.postimg.cc/WzQ1smNx/new-tfkhdyt-small-banner.jpg)](https://tfkhdyt.my.id/)

<p align=center>
  <a rel="me" href="https://fosstodon.org/@tfkhdyt">
    <img height="28" src="https://upload.wikimedia.org/wikipedia/commons/4/48/Mastodon_Logotype_%28Simple%29.svg" />
  </a>&nbsp;
  <a rel="me" href="https://matrix.to/#/@tfkhdyt:matrix.org">
    <img height="28" src="https://cdn.fosstodon.org/custom_emojis/images/000/435/156/original/a99cd4ef25ea1d9a.png" />
  </a>&nbsp;
  <a href="https://t.me/tfkhdyt">
    <img height="28" src="https://upload.wikimedia.org/wikipedia/commons/8/83/Telegram_2019_Logo.svg" />
  </a>&nbsp;
  <a href="https://facebook.com/tfkhdyt142">
    <img height="28" src="https://upload.wikimedia.org/wikipedia/commons/5/51/Facebook_f_logo_%282019%29.svg" />
  </a>&nbsp;
  <a href="https://youtube.com/tfkhdyt">
    <img height="28" src="https://upload.wikimedia.org/wikipedia/commons/a/a0/YouTube_social_red_circle_%282017%29.svg" />
  </a>&nbsp;
  <!-- <a href="https://twitter.com/tfkhdyt"><img height="28" src="https://upload.wikimedia.org/wikipedia/commons/4/4f/Twitter-logo.svg"></a>&nbsp; -->
  <a href="https://www.linkedin.com/mwlite/in/taufik-hidayat-6793aa200"><img height="28" src="https://upload.wikimedia.org/wikipedia/commons/8/81/LinkedIn_icon.svg"></a>&nbsp;
  <a href="https://instagram.com/_tfkhdyt_"><img height="28" src="https://upload.wikimedia.org/wikipedia/commons/e/e7/Instagram_logo_2016.svg"></a>&nbsp;
  <!-- <a href="https://pddikti.kemdikbud.go.id/data_mahasiswa/QUUyNzdEMjktNDk0Ri00RTlDLUE4NzgtNkUwRDBDRjIxOUNB"><img height="28" src="https://i.postimg.cc/YSB2c3DG/1619598282440.png"></a> -->
</p>
<p align="center">
  <img src="https://visitor-badge.laobi.icu/badge?page_id=tfkhdyt.tfkhdyt" />
  <a href="https://github.com/tfkhdyt"><img src="https://img.shields.io/github/followers/tfkhdyt?label=followers&style=social"/></a>
  <!-- <a href='https://stackshare.io/tfkhdyt/mn3'> -->
  <!--   <img src='http://img.shields.io/badge/tech-stack-0690fa.svg?style=flat' alt='StackShare' /> -->
  <!-- </a> -->
</p>

### Junior Full Stack Developer | Computer Science Student | FLOSS Enthusiast | GNU/Linux Geek

### About Me 👨🏻

- 👨🏻‍💼 My name is `Taufik Hidayat`
- 🏠 Live in `Bandung, Indonesia`
- 👶🏻 Born in `Majalengka, April 1st 2002`
- 🧍🏻‍♂️ `20` years old
- 💻 `Computer Science` Student at `Universitas Bale Bandung`
- 🌟 Currently, `TypeScript` and `Golang` are my favorite programming languages.
- ~~👨🏻‍💻 MN3 Stack Developer (`MongoDB`, `Nest.js`, `Next.js`, `Node.js`)~~
- 👨🏻‍💻 PNG Stack Developer (`PostgreSQL`, `Next.js`, `Gin`)
- 🌏 Languages
  - 🇮🇩 Indonesian
  - 🇮🇩 Sundanese
  - 🇬🇧 English

### Tech Stack 👨🏻‍💻

<span>
  <img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Unofficial_JavaScript_logo_2.svg" height="30" title="JavaScript" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/4c/Typescript_logo_2020.svg" height="30" title="TypeScript" />
  <img src="https://cdn.worldvectorlogo.com/logos/go-logo-1.svg" height="30" title="Golang" />
  <img src="https://www.vectorlogo.zone/logos/reactjs/reactjs-icon.svg" height="30" title="React" />
  <img src="https://www.tfkhdyt.my.id/_next/image?url=%2Fimages%2Ftech%2Fjotai.png&w=3840&q=100" height="30" title="Jotai" />
  <!-- <img src="https://upload.wikimedia.org/wikipedia/commons/1/10/Cib-next-js_%28CoreUI_Icons_v1.0.0%29.svg" height="30" title="Next.js" /> -->
  <img src="https://nextjs.org/static/favicon/favicon-32x32.png" height="30" title="Next.js" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Tailwind_CSS_Logo.svg" height="30" title="Tailwind CSS" />
  <img src="https://i.postimg.cc/1zY3VDPL/mantine.png" height="30" title="Mantine UI" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/b/b2/Bootstrap_logo.svg" height="30" title="Bootstrap" />
  <img src="https://www.vectorlogo.zone/logos/nodejs/nodejs-icon.svg" height="30" title="Node.js" />
  <img src="https://expressjs.com/images/favicon.png" height="30" title="Express.js" />
  <img src="https://docs.nestjs.com/assets/logo-small.svg" height="30" title="NestJS" />
  <img src="https://telegraf.js.org/media/logo.svg" height="30" title="Telegraf" />
  <img src="https://raw.githubusercontent.com/gin-gonic/logo/master/color.svg" height="30" title="Gin" />
  <img src="https://cdn.freebiesupply.com/logos/large/2x/linux-tux-1-logo-png-transparent.png" height="30" title="GNU/Linux" />
  <img src="images/icons/mongo.svg" height="30" title="MongoDB" />
  <img src="https://www.silicon.de/wp-content/uploads/2014/12/MariaDB-reflex-blue-seal-blue-lettering-below-600px.png" height="30" title="MariaDB" />
  <img src="https://www.vectorlogo.zone/logos/postgresql/postgresql-icon.svg" height="30" title="PostgreSQL" />
  <img src="https://www.svgrepo.com/show/303460/redis-logo.svg" height="30" title="Redis" />
  <img src="https://www.svgrepo.com/show/353659/docker-icon.svg" height="30" title="Docker" />
</span>

### Basic Experience 📖

<span>
  <img src="images/icons/jquery.svg" height="30" title="jQuery" />
  <img src="https://raw.githubusercontent.com/tfkhdyt/web-portfolio/main/public/icons/java.svg" height="30" title="Java" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg" height="30" title="Python" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg" height="30" title="C++" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/2/27/PHP-logo.svg" height="30" title="PHP" />
  <img src="https://wiki.freepascal.org/images/f/fd/Lazarus-icons-lpr-proposal-bpsoftware.png" height="30" title="Pascal" />
  <img src="https://upload.wikimedia.org/wikipedia/commons/d/d5/Rust_programming_language_black_logo.svg" height="30" title="Rust" />
</span>

### Equipment 🧰

- `Laptop` - Lenovo Ideapad Slim 3
  - `CPU` - AMD Ryzen 3 5300U
  - `RAM` - 8GB DDR4 3200MHz
  - `GPU` - AMD Vega 6
  - `Operating System` - GNU/Linux
    - `Distro` - EndeavourOS
    - `Kernel` - 6.0
    - `WM` - i3-gaps
- `Text Editor` - Neovim (LunarVim)
- `Browser` - Firefox, Brave
- `Terminal` - Alacritty
- `Shell` - zsh
- `Mobile Phone` - Realme 3 Pro
  - `Operating System` - Android 13
    - `Custom ROM` - Nusantara Project 5.1
    - `Kernel` - Stock

### Stats 📋

<p align="center">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=tfkhdyt&show_icons=true&theme=tokyonight&include_all_commits=true&count_private=true" />
  <img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=tfkhdyt&count_private=true&theme=tokyonight" />
  <img align="center" src="https://github-readme-stats.vercel.app/api/wakatime?username=tfkhdyt&theme=tokyonight&layout=compact" />
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=tfkhdyt&langs_count=10&theme=tokyonight&layout=compact&hide=css,scss,less,html,hack" />
</p>

### Support 💰

Press the button below to support me via donation

<p align="center">
  <a href="https://donate.tfkhdyt.my.id/">
    <img src="https://i.postimg.cc/jjRDbZQx/1621036430601.png" width="125px">
  </a>
</p>
